const mongoose = require('mongoose');
const {nanoid}  = require('nanoid');
const shortId = require('shortid');
const Schema = mongoose.Schema;


const LinkSchema = new Schema({
  _id: String,
   originalUrl: {
    type: String,
  },
  shortUrl : {
    default: shortId(),
     type: String,

  }

});

const Link = mongoose.model('Link', LinkSchema);

module.exports = Link;
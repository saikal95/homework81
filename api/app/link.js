const express = require('express');
const router = express.Router();
const shortId = require('shortid');
const { nanoid } = require('nanoid');
const Link = require("../models/Link");



router.get('/:shortUrl', async (req, res, next) => {
  try {
    const query = {};
    const sort = {};

    const links = await Link.find(query).sort(sort);

    return res.status(403).redirect('links');

  } catch (e) {
    next(e);
  }
});

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    const sort = {};

    const links = await Link.find(query).sort(sort);

    return res.send({message: 'New shorter URL for Link', links});

  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {

    const linkData = {
      _id:nanoid(),
      originalUrl: req.body.originalUrl,
      shortUrl: `localhost:8000/'${shortId()}`,
     };

    const link = new Link(linkData);

    await link.save();

    return res.send( `New shorter URL for Link', ${link}`);
  } catch (e) {
    next(e);
  }
});




module.exports = router;